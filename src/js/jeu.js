import { Player } from "./classuser"; 
import { Root } from "./classroot"; 
import css from "../css/style.scss";
import {win, lose} from "./score";
import '../../node_modules/bootstrap/scss/bootstrap.scss';

// INPUT NAME START
document.querySelector("#btnName").addEventListener('click',  function() {

let valueName = document.querySelector("#valuename").value;
let name = document.querySelector("#name"); 
name.textContent = valueName;
document.querySelector("#valuename").value = '';

let choice = document.querySelector('#msg-choice');
choice.textContent = 'Choississez un élément ! ';
choice.classList.add("jackInTheBox")
});

let verifRoot = '';
let verifUser='';
let scoreUser= 0;
let scoreRoot= 0;

// Function Win/lose Game 
function winLose() {
  let div = document.createElement("div");
  let text = document.createElement("p");
  let i = document.createElement("i");
  let msg = document.createElement("div");
  let divP = document.querySelector("#resultWinLose")

  div.classList.add("d-flex", "justify-content-between", "flex-row");
  text.classList.add("d-flex", "justify-content-end", "mb-auto", "mt-auto");
  msg.classList.add("d-flex", "justify-content-between");
 
  divP.appendChild(div)
  div.appendChild(msg);
  msg.appendChild(i);
  msg.appendChild(text);
  

  if (verifUser === "papier" && verifRoot === "caillou" || verifUser === "caillou" && verifRoot === "ciseaux" || verifUser === "ciseaux" && verifRoot=== "papier") {
    div.setAttribute("id", "msg-win");
    text.textContent = "Tu as battu l'ordi !";
    i.classList.add("fas", "fa-trophy", "justify-self-start");
    i.setAttribute("id", "icone-win");
    msg.classList.add("win");
    text.classList.add("txt-win");
    div.classList.add("title")
    scoreUser++;
    finalScore();

  }

 else if (verifRoot === verifUser) {
    div.setAttribute("id", "msg-retry");
    text.textContent = "Personne gagne, allez réessaye !";
    i.classList.add("far", "fa-frown", "justify-self-start");
    i.setAttribute("id", "icone-retry");
    msg.classList.add("retry");
    text.classList.add("txt-retry");

  } 

  else if(verifUser === "caillou" && verifRoot === "papier" || verifUser === "ciseaux" && verifRoot === "caillou" || verifUser === "papier" && verifRoot=== "ciseaux") {
    div.setAttribute("id", "msg-lose");
    text.textContent = "Tu as perdu contre un ordi, Bouhh!";
    i.classList.add("fas", "fa-spinner", "justify-self-start");
    i.setAttribute("id", "icone-lose");
    msg.classList.add("lose");
    text.classList.add("txt-lose");
    div.classList.add("bounceInUp")
    scoreRoot++;
    finalScore();

  }
};

// function display score
function score() {
  document.querySelector('#score-user').textContent = scoreUser;
  document.querySelector('#score-root').textContent = scoreRoot;
}


// function display message win or lose
function finalScore() {
  if (scoreUser === 3) {
    win();
  }
  else if (scoreRoot === 3) {
    lose();
  }
}



// GAME 
let buttonPaper = document.querySelector('#btn-paper')
let buttonRock = document.querySelector('#btn-rock')
let buttonCissors = document.querySelector('#btn-cissors')
let newPlayer = new Player();
let newOrdi = new Root();


buttonPaper.addEventListener('click', function () {
  remove();
  verifUser = "papier";
  newPlayer.insertPaper();
  newOrdi.insertRandom();
  verifRoot = newOrdi.verifRoot;
  winLose();
  score();
});

buttonRock.addEventListener('click', function () {
  remove();
  verifUser = "caillou"; 
  newPlayer.insertRock();
  newOrdi.insertRandom();
  verifRoot = newOrdi.verifRoot;
  winLose();
  score();
});

buttonCissors.addEventListener('click', function () {
  remove();
  verifUser = "ciseaux";
  newPlayer.insertCissors();
  newOrdi.insertRandom(); 
  verifRoot = newOrdi.verifRoot;
  winLose();
  score();
});

// function remove msg win,lose, retry
function remove() {
  let win = document.querySelector("#msg-win");
  let lose = document.querySelector("#msg-lose");
  let retry = document.querySelector("#msg-retry");
  if(win) {
    document.querySelector("#msg-win").remove();
  }
  else if (lose) {
    document.querySelector("#msg-lose").remove();
  }
   else if (retry){
    document.querySelector("#msg-retry").remove();
   }
};





